package com.interview.piglatin.service;

import org.springframework.stereotype.Service;

import javax.sound.midi.Soundbank;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class PigLatinService {

    public String pigLatinRules(final String phrase){
        char[] phraseChar = phrase.toCharArray();

        if(isVowel(phraseChar[0])){
            return phrase + "way";
        }
        else if(!isNumber(phrase)){

            if (phraseChar.length>=1 && !isVowel(phraseChar[1]) && isConsonant(String.valueOf(phraseChar,0,2))){

                return String.valueOf(phraseChar, 2, phraseChar.length-2) +
                        String.valueOf(phraseChar, 0,2) +
                        "ay";
            }
            else if (isConsonant(String.valueOf(phraseChar[0]))){

                    return String.valueOf(phraseChar,1, phraseChar.length-1) +
                            String.valueOf(phraseChar,0,1) +
                            "ay";
                }
        }
        return null;
    }


    private boolean isVowel(final char phraseChar){
        List<String> vowel = Arrays.asList("a","e","i","o","u","A","E","I","O","U");

        if(vowel.contains(String.valueOf(phraseChar))){
            return true;
        }
        return false;
    }

    private boolean isConsonant(final String phrase){
        Pattern pattern = Pattern.compile("[b-df-hj-np-tv-zB-DF-HJ-NP-TV-Z]");
        char[] consonantChar = phrase.toCharArray();

        for(int i=0; i<consonantChar.length; i++){
            Matcher matcher = pattern.matcher(String.valueOf(consonantChar[i]));
            if(!matcher.find()){
                return false;
            }
        }

        return true;
    }

    private boolean isNumber(final String fullPhrase){
        Pattern pattern = Pattern.compile("[0-9]");
        Matcher matcher = pattern.matcher(fullPhrase);

        if(matcher.find()){
            return true;
        }
        return false;
    }
}
