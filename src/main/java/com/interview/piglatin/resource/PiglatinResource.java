package com.interview.piglatin.resource;

import com.interview.piglatin.beans.PigLatinBean;
import com.interview.piglatin.exception.PigLatinException;
import com.interview.piglatin.service.PigLatinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/pig-latin")
public class PiglatinResource {

    @Autowired
    private PigLatinService pigLatinService;

    @GetMapping("/phrase/{phrase}")
    public PigLatinBean getPigLatin(@PathVariable String phrase){

        String pigLatinPhrase = pigLatinService.pigLatinRules(phrase);

        if(pigLatinPhrase == null){
            throw new PigLatinException("There was an exception processing the Phrase, Invalid Phrase!!!");
        }
        return new PigLatinBean(phrase, pigLatinPhrase);
    }
}
