package com.interview.piglatin.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class PigLatinException extends RuntimeException{

    public PigLatinException(final String message){
        super(message);
    }
}
