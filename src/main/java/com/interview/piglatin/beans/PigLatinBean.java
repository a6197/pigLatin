package com.interview.piglatin.beans;

import lombok.Data;

@Data
public class PigLatinBean {

    private String initialPhrase;
    private String PigLatinMessage;

    public PigLatinBean(String initialPhrase, String PigLatinMessage){
        this.initialPhrase = initialPhrase;
        this.PigLatinMessage = PigLatinMessage;
    }
}
