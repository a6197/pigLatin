package com.interview.piglatin.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class PigLatinServiceTest {

    PigLatinService pigLatinService;

    @BeforeEach
    public void setUp(){
        pigLatinService = new PigLatinService();
    }

    @Test
    public void pigLatinRulesTest(){

        //If a word starts with a vowel add the word "way" at the end of the word.
        String vowelRule = pigLatinService.pigLatinRules("awesome");
        assertEquals("awesomeway", vowelRule, "this phrase starts with a bowel, expected phrase should be the input + way");

        //If a word starts with a consonant and a vowel, put the first letter of the word at the end of the word and add "ay."
        String firstConsonant = pigLatinService.pigLatinRules("Happy");
        assertEquals("appyHay", firstConsonant,
                "this phrase starts with a consonant and a vowel, expected phrase should be the first letter of the word at the end of the word and add ay");

        //If a word starts with two consonants, move the two consonants to the end of the word and add ay
        String SecondConsonant = pigLatinService.pigLatinRules("child");
        assertEquals("ildchay", SecondConsonant, "If a word starts with two consonants, move the two consonants to the end of the word and add ay");
    }

}