the purpose of this project is to complete the code challenge 

Pig Latin Code challenge:
Rules.
Most words in Pig Latin end in "ay." Use the rules below to translate normal English into Pig Latin.
• If a word starts with a consonant and a vowel, put the first letter of the word at the end of the
word and add "ay."
Example: Happy = appyh + ay = appyhay
• If a word starts with two consonants, move the two consonants to the end of the word and add
"ay."
Example: Child = Ildch + ay = Ildchay
• If a word starts with a vowel add the word "way" at the end of the word.
Example: Awesome = Awesome +way = Awesomeway

Swagger Docs URL: http://localhost:8080/v2/#/

Swagger UI URL: http://localhost:8080/v2/api-docs
